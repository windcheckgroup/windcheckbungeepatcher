package ru.will0376.windcheckpatcher;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.md_5.bungee.protocol.AbstractPacketHandler;
import net.md_5.bungee.protocol.ProtocolConstants;
import net.md_5.bungee.protocol.packet.LoginSuccess;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LoginSuccessCustom extends LoginSuccess {
	private String uuid;
	private String username;
	private String data;

	@Override
	public void read(ByteBuf buf) {
		uuid = readString(buf);
		username = readString(buf);
		if (buf.isReadable()) data = readString(buf);
	}

	@Override
	public void read(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
		read(buf);
	}

	@Override
	public void write(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
		write(buf);
	}

	@Override
	public void write(ByteBuf buf) {
		writeString(uuid, buf);
		writeString(username, buf);

		if (data != null && !data.isEmpty()) writeString(data, buf);
	}

	@Override
	public void handle(AbstractPacketHandler handler) throws Exception {
		handler.handle(this);
	}
}
