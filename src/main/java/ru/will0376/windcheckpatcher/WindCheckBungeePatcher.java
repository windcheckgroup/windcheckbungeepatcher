package ru.will0376.windcheckpatcher;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.protocol.Protocol;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public final class WindCheckBungeePatcher extends Plugin {

	@Override
	public void onEnable() {
		try {
			Field to_client = Protocol.class.getDeclaredField("TO_CLIENT");
			to_client.setAccessible(true);
			Object o1 = to_client.get(Protocol.LOGIN);

			Class<?> aClass = Class.forName("net.md_5.bungee.protocol.Protocol$ProtocolMapping");
			Constructor<?> declaredConstructor = aClass.getDeclaredConstructor(Integer.TYPE, Integer.TYPE);
			declaredConstructor.setAccessible(true);

			List<Method> registerPacket1 = Arrays.stream(o1.getClass().getDeclaredMethods())
					.filter(e -> e.getName().equals("registerPacket") && e.getParameters().length == 2)
					.collect(Collectors.toList());

			Method registerPacket = registerPacket1.get(0);
			registerPacket.setAccessible(true);

			Object o2 = Array.newInstance(aClass, 1);
			Array.set(o2, 0, declaredConstructor.newInstance(ProtocolConstants.MINECRAFT_1_8, 0x02));

			registerPacket.invoke(o1, LoginSuccessCustom.class, o2);

		} catch (Exception e) {
			e.printStackTrace();
			ProxyServer.getInstance().stop();
		}

	}
}
